#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Copyright 2018 Ángel G.
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


def main(args):
	bulb_ip = "192.168.1.139"
	bulb_port = 55443
	
	method = "start_cf"
	params = "0,2,\"5000,1,16711680,100,5000,1,16776960,100,5000,1,65280,100,5000,1,255,100,5000,1,16711935,100\""

	tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	print("connect" + " " + bulb_ip + ":" + str(bulb_port) + " " + "...")
	tcp_socket.connect((bulb_ip, int(bulb_port)))
	msg = "{\"id\":" + str(1) + ",\"method\":\""
	msg += method + "\",\"params\":[" + params + "]}\r\n"
	tcp_socket.send(msg.encode('utf-8'))
	print(msg)
	tcp_socket.close()
	return 0

if __name__ == '__main__':
    import sys
    import socket
    import time
    sys.exit(main(sys.argv))
